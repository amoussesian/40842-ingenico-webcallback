﻿using System.Diagnostics;
using System.ServiceModel;
using System.ServiceProcess;

namespace ININ.PSO.EMEA.WebCallback.Service
{
    public partial class WebCallService : ServiceBase
    {
        ServiceHost _serviceHost;

        /// <summary>
        /// 
        /// </summary>
        public WebCallService()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        #region Service Start/Stop
        protected override void OnStart(string[] args)
        {
            try
            {
                LogMessage(EventLogEntryType.Information, "Try to start Service");
                if (_serviceHost != null)
                    _serviceHost.Close();
                _serviceHost = new ServiceHost(typeof(ScheduleCallbackService));
                _serviceHost.Opening += serviceHost_Opening;
                _serviceHost.Opened += serviceHost_Opened;
                _serviceHost.Closing += serviceHost_Closing;
                _serviceHost.Closed += serviceHost_Closed;
                _serviceHost.Faulted += serviceHost_Faulted;
                _serviceHost.Open();
                LogMessage(EventLogEntryType.Information, "Service started successfully");
            }
            catch (System.Exception ex)
            {
                LogMessage(EventLogEntryType.Error, "Exception while trying to start service: " + ex.Message + " " + ex.InnerException);
            }
        }

        void serviceHost_Faulted(object sender, System.EventArgs e)
        {
            LogMessage(EventLogEntryType.Error, "Service is in a faulted state. You need to restart the windows service");
        }

        void serviceHost_Closed(object sender, System.EventArgs e)
        {
            LogMessage(EventLogEntryType.Information, "Service is now closed");
        }

        void serviceHost_Closing(object sender, System.EventArgs e)
        {
            LogMessage(EventLogEntryType.Information, "Service is closing");
        }

        void serviceHost_Opened(object sender, System.EventArgs e)
        {
            LogMessage(EventLogEntryType.Information, "Service is now opened");
        }

        void serviceHost_Opening(object sender, System.EventArgs e)
        {
            LogMessage(EventLogEntryType.Information, "Service is opening");
        }
        /// <summary>
        /// 
        /// </summary>
        protected override void OnStop()
        {
            if (_serviceHost != null)
            {
                _serviceHost.Close();
                _serviceHost = null;
            }
        }
        #endregion

        private void LogMessage(EventLogEntryType entryType, string message)
        {
            var eventSource = ServiceName;
            const string eventLog = "Application";

            if (!EventLog.SourceExists(eventSource))
                EventLog.CreateEventSource(eventSource, eventLog);

            EventLog.WriteEntry(eventSource, message, entryType);
        }
    }
}
