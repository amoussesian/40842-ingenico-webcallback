﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;


namespace ININ.WindowsService.WebCallback
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        class Configration
        {
            public string Cicserver;
            public string Cicuser;
            public string Cicpassword;
            public string Cicworkgroup;
            public string Sqlserver;
            public string Sqluser;
            public string Sqlpassword;
        }

        private Configration _prod = new Configration
        {
            Cicserver = "wrinincic11",
            Cicuser = "cicexadm",
            Cicpassword = "4kv0wkeb",
            Cicworkgroup = "A_cb_B2C_IBE_Service",
            Sqlserver = "ininsql-cluster",
            Sqluser = "ic_user",
            Sqlpassword = "Se!nus448"
        };

        private Configration _test = new Configration
        {
            Cicserver = "wrinincic21",
            Cicuser = "cicexadm",
            Cicpassword = "4kv0wkeb",
            Cicworkgroup = "A_cb_B2C_IBE_Service",
            Sqlserver = "sql-testcluster",
            Sqluser = "ic_user",
            Sqlpassword = "Se!nus448t"
        };
 
        /// <summary>
        /// 
        /// </summary>
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        protected override void OnBeforeUninstall(IDictionary savedState)
        {
            try
            {
                StopService(true, "OnBeforeUninstall");
            }
            catch (Exception ex)
            {
                logMessage(string.Concat("Error: ", ex.Message), EventLogEntryType.Error, "OnBeforeUninstall");
            }
            base.OnBeforeUninstall(savedState);
        }

        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);
            string targetPath = Context.Parameters["assemblypath"];
            string param1 = Context.Parameters["param1"];
            string param2 = Context.Parameters["param2"];
            string param3 = Context.Parameters["param3"];
            string param4 = Context.Parameters["param4"];
            string param5 = Context.Parameters["param5"];
            string param6 = Context.Parameters["param6"];

            //System.Diagnostics.Debugger.Break();

            Configuration config = ConfigurationManager.OpenExeConfiguration(targetPath);
            config.AppSettings.Settings["ICServer"].Value = param1;
            config.AppSettings.Settings["ICUsername"].Value = param2;
            config.AppSettings.Settings["ICPassword"].Value = param3;
            config.AppSettings.Settings["CallbackWorkgroup"].Value = param4;
            config.AppSettings.Settings["SQLConnectionString"].Value = param5;
            config.AppSettings.Settings["DatabaseAccessInterval"].Value = param6;

            config.Save();
        }

        protected override void OnAfterInstall(System.Collections.IDictionary savedState)
        {
            base.OnAfterInstall(savedState);
            this.logMessage("Service will be started.", EventLogEntryType.Information, "OnAfterInstall");
            using (var serviceController = new ServiceController(this.serviceInstaller1.ServiceName, Environment.MachineName))
                serviceController.Start();
        }

        private Boolean StopService(Boolean bLogError, String sContext)
        {
            var controller = new ServiceController(serviceInstaller1.ServiceName);
            try
            {
                if (controller.Status == ServiceControllerStatus.Running | controller.Status == ServiceControllerStatus.Paused)
                {
                    this.logMessage("Service is running, will be stopped now.", EventLogEntryType.Information, sContext);
                    controller.Stop();
                    controller.WaitForStatus(ServiceControllerStatus.Stopped, new TimeSpan(0, 0, 0, 30));
                    controller.Close();
                    this.logMessage("Service is stopped now.", EventLogEntryType.Information, sContext);
                }
                else
                {
                    this.logMessage("Service is not running, no need to stop it.", EventLogEntryType.Information, sContext);
                }
                return true;
            }
            catch (Exception ex)
            {
                if (bLogError)
                    logMessage(
                        String.Format("{0} could not be stopped. Please stop the service manually. Error: {1}",
                            serviceInstaller1.ServiceName, ex.Message), EventLogEntryType.Error, sContext);
                else
                    logMessage(
                        String.Format("No previous installation from Service {0} found, no need to stop it.",
                            serviceInstaller1.ServiceName), EventLogEntryType.Information, sContext);
                return false;
            }
        }

        private Boolean IsServiceInstalled()
        {
            var controller = new ServiceController(serviceInstaller1.ServiceName);
            try
            {
                if (controller.Status == ServiceControllerStatus.Running | controller.Status == ServiceControllerStatus.Paused)
                {
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        private void logMessage(string sMessage, EventLogEntryType eventType, string sContext)
        {
            string eventSource = serviceInstaller1.ServiceName;
            string eventLog = "Application";
            if (!EventLog.SourceExists(eventSource))
            {
                EventLog.CreateEventSource(eventSource, eventLog);
            }
            EventLog eLog = new EventLog();
            eLog.Source = eventSource;
            eLog.WriteEntry(sContext + ": " + sMessage, eventType);
        }
    }
}
