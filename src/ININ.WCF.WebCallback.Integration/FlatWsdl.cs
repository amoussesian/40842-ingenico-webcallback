using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Xml.Schema;
using ServiceDescription = System.Web.Services.Description.ServiceDescription;

namespace ININ.PSO.EMEA.WebCallback
{
    /// <summary>
    /// Used to render the WSDL as a single file as opposed to WCF default which outputs multiple WSDL files, making them incompatible with service discovery.
    /// </summary>
    public class FlatWsdl : BehaviorExtensionElement, IWsdlExportExtension, IEndpointBehavior
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="exporter"></param>
        /// <param name="context"></param>
        public void ExportContract(WsdlExporter exporter, WsdlContractConversionContext context)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="exporter"></param>
        /// <param name="context"></param>
        public void ExportEndpoint(WsdlExporter exporter, WsdlEndpointConversionContext context)
        {
            var schemaSet = exporter.GeneratedXmlSchemas;

            foreach (ServiceDescription wsdl in exporter.GeneratedWsdlDocuments)
            {
                var importsList = new List<XmlSchema>();

                foreach (XmlSchema schema in wsdl.Types.Schemas)
                {
                    AddImportedSchemas(schema, schemaSet, importsList);
                }

                wsdl.Types.Schemas.Clear();

                foreach (var schema in importsList)
                {
                    RemoveXsdImports(schema);
                    wsdl.Types.Schemas.Add(schema);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema"></param>
        /// <param name="schemaSet"></param>
        /// <param name="importsList"></param>
        private static void AddImportedSchemas(XmlSchema schema, XmlSchemaSet schemaSet, List<XmlSchema> importsList)
        {
            foreach (var ixsd in schema.Includes.Cast<XmlSchemaImport>().Select(import => schemaSet.Schemas(import.Namespace)).SelectMany(realSchemas => realSchemas.Cast<XmlSchema>().Where(ixsd => !importsList.Contains(ixsd))))
            {
                importsList.Add(ixsd);
                AddImportedSchemas(ixsd, schemaSet, importsList);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema"></param>
        private static void RemoveXsdImports(XmlSchema schema)
        {
            for (var i = 0; i < schema.Includes.Count; i++)
            {
                if (schema.Includes[i] is XmlSchemaImport)
                    schema.Includes.RemoveAt(i--);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="bindingParameters"></param>
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="clientRuntime"></param>
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="endpointDispatcher"></param>
        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="endpoint"></param>
        public void Validate(ServiceEndpoint endpoint)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        public override System.Type BehaviorType
        {
            get { return typeof(FlatWsdl); }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override object CreateBehavior()
        {
            return new FlatWsdl();
        }
    }
}