﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Threading;
using ININ.IceLib.Connection;
using ININ.IceLib.Interactions;
using ININ.IceLib.People;
using ININ.PSO.EMEA.WebCallback.Classes;

namespace ININ.PSO.EMEA.WebCallback
{
    /// <summary>
    /// This service includes multiple functionalities for creating Callback and to gather Statistics and Businesshours for Workgroups.
    /// </summary>
    [ServiceBehavior(Namespace = "http://inin.com", InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    [ErrorHandler]
    public class ScheduleCallbackService : IScheduleCallbackService
    {
        #region -= Properties & Variables =-

        /// <summary>
        /// 
        /// </summary>
        private const string CUSTOM_NOTIFY_PHONE_NUMBER_VALIDATION_SND = "CheckPhoneNumber_Snd";

        /// <summary>
        /// 
        /// </summary>
        private const string CUSTOM_NOTIFY_PHONE_NUMBER_VALIDATION_RECV = "CheckPhoneNumber_Recv";

        /// <summary>
        /// The _custom notification manager
        /// </summary>
        private CustomNotificationManager _customNotificationManager;

        /// <summary>
        /// Occurs when [on custom notification handled].
        /// </summary>
        public event EventHandler<CustomNotificationHandledEventArgs> OnCustomNotificationHandled;

        private readonly AutoResetEvent _notificationReceivedResetEvent;

        private CustomNotificationHandledEventArgs _lastNotificationResult;

        /// <summary>
        /// The _auto connect session
        /// </summary>
        private readonly AutoConnectSession _autoConnectSession;

        /// <summary>
        /// The _bool stop
        /// </summary>
        private bool _boolStop;

        /// <summary>
        /// The _schedules
        /// </summary>
        private readonly Schedules _schedules;

        /// <summary>
        /// The _session map
        /// </summary>
        private readonly Dictionary<string, Session> _sessionMap = new Dictionary<string, Session>();

        /// <summary>
        /// The _database access interval
        /// </summary>
        private readonly int _databaseAccessInterval;

        #endregion

        #region -= Constructor & Initialization =-

        /// <summary>
        /// WCF service constructor
        /// </summary>
        public ScheduleCallbackService()
        {
            Tracing.Initialize("CallbackService");

            using (Tracing.CIC.scope())
            {
                try
                {
                    Tracing.CIC.status("ININ.EMEA.WebCallback version {} is starting.",
                        Assembly.GetExecutingAssembly().GetName().Version);

                    _notificationReceivedResetEvent = new AutoResetEvent(false);

                    _autoConnectSession = AutoConnectSession.GetInstance();

                    _autoConnectSession.OnConnected += AutoConnectSessionConnected;

                    _autoConnectSession.ConnectionStateChange += AutoConnectSessionConnectionStateChange;

                    _autoConnectSession.ConnectSession();

                    var connectionString = System.Configuration.ConfigurationManager.AppSettings["SQLConnectionString"];

                    Tracing.CIC.status("Connection string: {}", connectionString);

                    _schedules = new Schedules(connectionString);

                    Tracing.CIC.status("Schedules created with connection string: {}", connectionString);

                    double interval;

                    // Get interval in seconds
                    if (
                        !Double.TryParse(
                            System.Configuration.ConfigurationManager.AppSettings["DatabaseAccessInterval"],
                            out interval)) interval = 10;

                    _databaseAccessInterval = (int) (interval*1000); //to ms

                    var workerThread = new Thread(CreateCallbacks);

                    Tracing.CIC.note("CreateCallbacks thread started");

                    workerThread.Start();

                    Tracing.CIC.note("Service has successfully started.");
                }
                catch (Exception e)
                {
                    Tracing.CIC.exception(e);
                }
            }
        }

        /// <summary>
        /// Automatics the connect session connected.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void AutoConnectSessionConnected(object sender, EventArgs e)
        {
            using (Tracing.CIC.scope())
            {
                try
                {
                    Tracing.CIC.status("Session is setup.");

                    if (_customNotificationManager == null)
                        _customNotificationManager = new CustomNotificationManager(_autoConnectSession.CurrentSession);

                    _customNotificationManager.StartWatching(new List<string>
                    {
                        CUSTOM_NOTIFY_PHONE_NUMBER_VALIDATION_RECV
                    });
                }
                catch (Exception ex)
                {
                    Tracing.CIC.exception(ex);
                }
            }
        }

        /// <summary>
        /// Invokes the on custom notification handled.
        /// </summary>
        /// <param name="e">The <see cref="CustomNotificationHandledEventArgs"/> instance containing the event data.</param>
        protected virtual void InvokeOnCustomNotificationHandled(CustomNotificationHandledEventArgs e)
        {
            var handler = OnCustomNotificationHandled;

            if (handler != null)
                handler(this, e);
        }

        /// <summary>
        /// Automatics the connect session connection state change.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="data">The <see cref="CustomConnectionStateChangeEventArgs"/> instance containing the event data.</param>
        void AutoConnectSessionConnectionStateChange(object sender, CustomConnectionStateChangeEventArgs data)
        {
            using (Tracing.CIC.scope())
            {
                try
                {
                    try
                    {
                        Tracing.CIC.note(String.Format("Session connection state changed to: {0}", data.ConnectionState));

                        if (data.ConnectionState == CustomConnectionState.Down || data.ConnectionState == CustomConnectionState.Unknown)
                        {
                            if(_customNotificationManager != null)
                                _customNotificationManager.StopWatching();

                            _sessionMap.Clear();
                        }
                    }
                    catch (Exception ex)
                    {
                        Tracing.CIC.exception(ex, "WCFService.cs:session_ConnectionStateChanged");
                    }
                }
                catch (Exception exception)
                {
                    Tracing.CIC.exception(exception);
                }
            }
        }

        private void CustomNotificationReceivedHandler(object sender, CustomNotificationEventArgs e)
        {
            using (Tracing.CIC.scope())
            {
                if(e.ObjectId != CUSTOM_NOTIFY_PHONE_NUMBER_VALIDATION_RECV) return;

                _lastNotificationResult = new CustomNotificationHandledEventArgs(e.Result.Length > 0, e.Result,
                    e.ObjectId);

                _notificationReceivedResetEvent.Set();
            }
        }

        /// <summary>
        /// Customs the notification received.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="CustomNotificationEventArgs"/> instance containing the event data.</param>
        private void CustomNotificationReceived(object sender, CustomNotificationEventArgs e)
        {
            using (Tracing.CIC.scope())
            {
                var success = false;

                var objectId = string.Empty;

                var result = new string[] { };

                try
                {
                    objectId = e.ObjectId;

                    Tracing.CIC.note("Custom notification received for object id: '" + objectId + "'.");

                    if (objectId != CUSTOM_NOTIFY_PHONE_NUMBER_VALIDATION_RECV)
                        return;

                    result = e.Result;

                    if (result.Length > 0)
                    {
                        Tracing.CIC.note("Received tructured paramater value: '" + result[0] + "'.");

                        success = true;
                    }
                    else
                        Tracing.CIC.warning("Custom notification result is empty!");
                }
                catch (Exception ex)
                {
                    Tracing.CIC.exception(ex);
                }

                InvokeOnCustomNotificationHandled(new CustomNotificationHandledEventArgs(success, result, objectId));
            }
        }

        /// <summary>
        /// WCF service destructor
        /// </summary>
        ~ScheduleCallbackService()
        {
            _boolStop = true;
        }
        
        #endregion

        #region -= CREATE CALLBACK =-

        /// <summary>
        /// Creates the callbacks.
        /// </summary>
        private void CreateCallbacks()
        {
            while (!_boolStop)
            {
                using (Tracing.CIC.scope("Time: {}", DateTime.Now.ToString("O")))
                {
                    try
                    {
                        var dt = _schedules.GetSchedules();

                        Tracing.CIC.verbose("Number of scheduled callbacks: {}", dt.Rows.Count);

                        foreach (DataRow row in dt.Rows)
                        {
                            var queue = row["QUEUE"].ToString();
                            var number = row["NUMBER"].ToString();
                            var message = row["MESSAGE"].ToString();
                            var attributes =
                                row["ATTRIBUTES"].ToString()
                                    .Split('|')
                                    .Select(attribute => attribute.Split(':'))
                                    .Where(strings => strings.Length >= 2);

                            var id = row["ID"].ToString();

                            Tracing.CIC.note(String.Format("Creating Callback from Schedule: {0} {1} {2}", id, queue,
                                number));

                            try
                            {
                                var callbackParameters = new CallbackParameters
                                {
                                    Message = message,
                                    Number = number,
                                    ScopedQueueName = queue
                                };

                                foreach (var attributeSplit in attributes)
                                {
                                    callbackParameters.Attributes.Add(attributeSplit[0], attributeSplit[1]);
                                }

                                var callback = CreateCallback(callbackParameters);

                                _schedules.UpdateSchedule(id, callback.CallbackId, 1);
                            }
                            catch (Exception e)
                            {
                                Tracing.CIC.error(String.Format("CreateCallbacks: {0} {1}", e.Message, e.StackTrace));
                                _schedules.UpdateSchedule(id, "ERROR", 0);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Tracing.CIC.error(String.Format("CreateCallbacks: {0} {1}", e.Message, e.StackTrace));
                    }

                    Thread.Sleep(_databaseAccessInterval);
                }
            }
        }

        /// <summary>
        /// Determines whether a scoped queue name exists in the CIC system.
        /// </summary>
        /// <param name="scopedQueueName">Name of the scoped queue.</param>
        /// <returns></returns>
        private bool QueueExists(string scopedQueueName)
        {
            using (Tracing.CIC.scope())
            {
                try
                {
                    var queueIdentifiers = scopedQueueName.Split(':');

                    var queueType = queueIdentifiers.FirstOrDefault();

                    if (queueType == null || (!queueType.Equals("User Queue", StringComparison.OrdinalIgnoreCase) &&
                        !queueType.Equals("Workgroup Queue", StringComparison.OrdinalIgnoreCase)))
                    {
                        Tracing.CIC.warning("Invalid queue type: {}", queueType);

                        return false;
                    }

                    var queueName = queueIdentifiers.LastOrDefault();

                    if (String.IsNullOrEmpty(queueName))
                    {
                        Tracing.CIC.warning("Invalid queue name: {}", queueName);

                        return false;
                    }

                    var peopleManager = PeopleManager.GetInstance(_autoConnectSession.CurrentSession);

                    var lookupParametersUser = new LookupParameters
                    {
                        ColumnsToMatch =
                            new List<LookupEntryProperty> {LookupEntryProperty.DisplayName, LookupEntryProperty.EntryId},
                        ComparisonType = LookupComparisonType.Exact,
                        DirectoriesToSearch = new List<LookupEntryType> {LookupEntryType.User},
                        IncludeUsersExcludedFromCompanyDirectory = true,
                        LookupString = queueName,
                        MaxEntries = 1
                    };

                    var lookupParametersWorkgroup = new LookupParameters
                    {
                        ColumnsToMatch =
                            new List<LookupEntryProperty> {LookupEntryProperty.DisplayName, LookupEntryProperty.EntryId},
                        ComparisonType = LookupComparisonType.Exact,
                        DirectoriesToSearch = new List<LookupEntryType> {LookupEntryType.Workgroup},
                        IncludeUsersExcludedFromCompanyDirectory = true,
                        LookupString = queueName,
                        MaxEntries = 1
                    };

                    var userEntries = peopleManager.GetLookupEntries(lookupParametersUser);

                    var workgroupEntries = peopleManager.GetLookupEntries(lookupParametersWorkgroup);

                    if (userEntries.LookupEntries.Count == 1 || workgroupEntries.LookupEntries.Count == 1)
                    {
                        Tracing.CIC.verbose("Queue {} exists.", queueName);

                        return true;
                    }

                    Tracing.CIC.warning("User entries: {}, Workgroup entries: {}, queue name: {}",
                        userEntries.LookupEntries.Count, workgroupEntries.LookupEntries.Count, queueName);

                    return false;
                }
                catch (Exception exception)
                {
                    Tracing.CIC.exception(exception);

                    return false;
                }
            }
        }

        /// <summary>
        /// Checks the number.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns></returns>
        /// <exception cref="System.ServiceModel.FaultException">Phoenumber + number +  has wrong format.
        /// or
        /// Provided number did not meet validation criteria.
        /// or
        /// Phoenumber custom handler validation failed.</exception>
        private string CheckNumber(string number)
        {
            using (Tracing.CIC.scope())
            {
                try
                {
                    var phonenumberRegex = new Regex("^\\+?[0-9]+$");

                    if (!phonenumberRegex.IsMatch(number))
                        throw new FaultException("Phoenumber" + number + " has wrong format.");

                    Tracing.CIC.note("Trying to send cutom notification request with object id: '"
                                     + CUSTOM_NOTIFY_PHONE_NUMBER_VALIDATION_SND + "', phone numer to check: '"
                                     + number + "'.");

                    var requestId = Guid.NewGuid().ToString("N");

                    _customNotificationManager.OnCustomNotificationReceived -= CustomNotificationReceivedHandler;

                    _customNotificationManager.OnCustomNotificationReceived += CustomNotificationReceivedHandler;

                    _customNotificationManager.SendCustomNotificationRequest(
                            CUSTOM_NOTIFY_PHONE_NUMBER_VALIDATION_SND,
                            new[]
                                {
                                        requestId,
                                        number
                                });

                    var success = _notificationReceivedResetEvent.WaitOne(TimeSpan.FromSeconds(5));

                    if (success)
                    {
                        if (!_lastNotificationResult.Success)
                            throw new FaultException("Phone number validation failed (Invalid result).");

                        if (
                            !_lastNotificationResult.Result[1].Equals("true",
                                StringComparison.InvariantCultureIgnoreCase))
                            throw new FaultException("Provided number did not meet validation criteria.");
                    }
                    else
                        throw new FaultException("Phone number validation failed (5 second timeout).");

                    return number.Replace("(", "")
                                 .Replace(")", "")
                                 .Replace("-", "")
                                 .Replace("/", "");
                }
                catch (Exception exception)
                {
                    Tracing.CIC.exception(exception);

                    throw;
                }
            }
        }

        /// <summary>
        /// Create a Callback at the agent queue. If the agent is unable to handle a Callback simultaneously it creates the required callback in the corresponding workgroup queue instead.
        /// </summary>
        /// <param name="parameters">The <see cref="CallbackParameters" />.</param>
        /// <returns>
        /// A <see cref="CreateCallbackResponse" /> object.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// parameters
        /// or
        /// parameters
        /// </exception>
        /// <exception cref="System.ServiceModel.FaultException">Connection is down
        /// or</exception>
        /// <exception cref="FaultException"></exception>
        public CreateCallbackResponse CreateCallback(CallbackParameters parameters)
        {
            if(parameters == null) throw new ArgumentNullException("parameters");

            using (Tracing.CIC.scope("({}, {})", parameters.ScopedQueueName, parameters.Number))
            {
                Tracing.CIC.verbose("Message: {}{}Attributes: {}", parameters.Message, Environment.NewLine,
                    parameters.Attributes == null
                        ? "[null]"
                        : parameters.Attributes.Aggregate(String.Empty,
                            (s, pair) => s + Environment.NewLine + pair.Key + ":" + pair.Value));

                if (String.IsNullOrEmpty(parameters.Number) || String.IsNullOrEmpty(parameters.ScopedQueueName))
                    throw new ArgumentNullException("parameters");

                parameters.Number = CheckNumber(parameters.Number);

                if (!QueueExists(parameters.ScopedQueueName))
                {
                    throw new FaultException("Queue " + parameters.ScopedQueueName +
                                             " does not exist or is not a valid scoped queue name.");
                }

                var result = new CreateCallbackResponse();

                Interaction callbackInteraction = null;

                try
                {
                    if (_autoConnectSession.CurrentSession.ConnectionState != IceLib.Connection.ConnectionState.Up)
                    {
                        Tracing.CIC.note("createCallback: Connection is down");

                        throw new FaultException("Connection is down");
                    }

                    var interactionsManager = InteractionsManager.GetInstance(_autoConnectSession.CurrentSession);

                    var callbackParameters = new CallbackInteractionParameters(parameters.ScopedQueueName,
                        parameters.Number, parameters.Message, parameters.Attributes ?? new Dictionary<string, string>());


                    callbackInteraction = interactionsManager.MakeCallbackInteraction(callbackParameters);

                    result.CallbackId = callbackInteraction.InteractionId.ToString();

                    Tracing.CIC.status("Callback created Callid: {}", result.CallbackId);

                    return result;
                }
                catch (Exception e)
                {
                    Tracing.CIC.exception(e);

                    throw new FaultException(e.Message);
                }
                finally
                {
                    if (callbackInteraction != null && callbackInteraction.IsWatching())
                        callbackInteraction.StopWatching();
                }
            }
        }

        /// <summary>
        /// Create a Callback schedule entry in the Table CallbackSchedules. A server component will frequently scan (set by a configurable parameter) the table CallbackSchedules looking for callbacks to initiate. If so it creates the required callbacks in the corresponding workgroup queue.
        /// </summary>
        /// <param name="parameters">The <see cref="CallbackParameters" /> used for scheduling a callback.</param>
        /// <returns>
        /// Reference to the Callback
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// parameters
        /// or
        /// parameters
        /// </exception>
        /// <exception cref="System.ServiceModel.FaultException">
        /// Queue  + parameters.ScopedQueueName +
        ///                                                   does not exist or is not a valid scoped queue name.
        /// or
        /// </exception>
        /// <exception cref="FaultException"></exception>
        public string CreateCallbackSchedule(CallbackParameters parameters)
        {
            if(parameters == null) throw new ArgumentNullException("parameters");

            using (Tracing.CIC.scope("({}, {}, {})", parameters.ScopedQueueName, parameters.Number, parameters.DateTime))
            {
                try
                {
                    if (String.IsNullOrEmpty(parameters.ScopedQueueName) || String.IsNullOrEmpty(parameters.Number))
                        throw new ArgumentNullException("parameters");

                    Tracing.CIC.verbose("Message: {}{}Attributes: {}", parameters.Message, Environment.NewLine,
                        parameters.Attributes == null
                            ? "[null]"
                            : parameters.Attributes.Aggregate(String.Empty,
                                (s, pair) => s + Environment.NewLine + pair.Key + ":" + pair.Value));

                    parameters.Number = CheckNumber(parameters.Number);

                    if (!QueueExists(parameters.ScopedQueueName))
                    {
                        throw new FaultException("Queue " + parameters.ScopedQueueName +
                                                 " does not exist or is not a valid scoped queue name.");
                    }

                    var attributeString = String.Empty;

                    if (parameters.Attributes != null && parameters.Attributes.Any())
                    {
                        attributeString = parameters.Attributes.Aggregate(String.Empty,
                            (s, pair) => s + pair.Key + ":" + pair.Value + "|");

                        attributeString = attributeString.Remove(attributeString.Length - 1); //Remove last character ('|')
                    }


                      _schedules.InsertSchedule(DateTimeOffset.Parse(parameters.DateTime).UtcDateTime, 
                                              parameters.Number,
                                              parameters.ScopedQueueName, 
                                              parameters.Message,
                                              attributeString,
                                              parameters.CustomValue1);
               

                    return "Success";
                }
                catch (Exception exception)
                {
                    Tracing.CIC.exception(exception);

                    throw new FaultException(exception.Message);
                }
            }
        }

        #endregion

        #region -= Dispose =-
        /// <summary>
        /// Always call this method before closing the connection with the web service.
        /// </summary>
        public void Dispose()
        {
            try
            {
                lock (_sessionMap)
                {
                    if (_sessionMap.Count <= 0) return;
                    foreach (var currentSession in _sessionMap.Select(session => _sessionMap[session.Key]))
                    {
                        if (currentSession.ConnectionState == IceLib.Connection.ConnectionState.Up)
                        {
                            currentSession.Disconnect();
                        }
                        currentSession.Dispose();
                    }
                    _sessionMap.Clear();
                }
            }
            catch (Exception ex)
            {
                Tracing.CIC.exception(ex, "Dispose");
            }
        }
        #endregion
    }
}
