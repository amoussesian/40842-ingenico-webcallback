﻿using System;

namespace ININ.PSO.EMEA.WebCallback.Classes
{
    /// <summary>
    /// 
    /// </summary>
    public class CustomNotificationHandledEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CustomNotificationEventArgs"/> is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public string[] Result { get; set; }

        /// <summary>
        /// Gets or sets the object id.
        /// </summary>
        /// <value>
        /// The object id.
        /// </value>
        public string ObjectId { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomNotificationHandledEventArgs"/> class.
        /// </summary>
        /// <param name="success">if set to <c>true</c> [success].</param>
        /// <param name="result">The result.</param>
        /// <param name="objectId">The object identifier.</param>
        public CustomNotificationHandledEventArgs(bool success,
                                                  string[] result,
                                                  string objectId)
        {
            Success = success;

            Result = result;

            ObjectId = objectId;
        }
    }
}
