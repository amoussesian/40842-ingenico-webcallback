﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Xml;
using System.Xml.Schema;

namespace ININ.PSO.EMEA.WebCallback.Classes
{
    /// <summary>
    /// This WCF behavior changes metadata generation for service contracts, so that operation
    /// parameters are required by default (XML schema minOccurs="1").
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface)]
    public class RequiredParametersBehaviorAttribute : Attribute, IContractBehavior, IWsdlExportExtension
    {
        private List<RequiredMessagePart> _requiredParameter;

        #region IContractBehavior Members (nothing to be done)

        void IContractBehavior.AddBindingParameters(ContractDescription contractDescription, ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        void IContractBehavior.ApplyClientBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
        }

        void IContractBehavior.ApplyDispatchBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
        {
        }

        void IContractBehavior.Validate(ContractDescription contractDescription, ServiceEndpoint endpoint)
        {
        }

        #endregion

        #region IWsdlExportExtension Members

        /// <summary>
        /// When ExportContract is called to generate the necessary metadata, we inspect the service
        /// contract and build a list of parameters that we'll need to adjust the XSD for later.
        /// </summary>
        void IWsdlExportExtension.ExportContract(WsdlExporter exporter, WsdlContractConversionContext context)
        {
            _requiredParameter = new List<RequiredMessagePart>();

            foreach (var operation in context.Contract.Operations)
            {
                var inputMessage = operation.Messages.First(m => m.Direction == MessageDirection.Input);
                var parameters = operation.SyncMethod.GetParameters();

                for (var i = 0; i < parameters.Length; i++)
                {
                    object[] attributes = parameters[i].GetCustomAttributes(typeof(OptionalAttribute), false);
                    if (attributes.Length == 0)
                    {
                        _requiredParameter.Add(new RequiredMessagePart
                        {
                            Namespace = inputMessage.Body.Parts[i].Namespace,
                            Message = operation.Name,
                            Name = inputMessage.Body.Parts[i].Name
                        });
                    }
                }
            }
        }

        /// <summary>
        /// When ExportEndpoint is called, the XML schemas have been generated. Now we can manipulate to
        /// our heart's content.
        /// </summary>
        void IWsdlExportExtension.ExportEndpoint(WsdlExporter exporter, WsdlEndpointConversionContext context)
        {
            if (_requiredParameter == null)
            {
                // If we have defined two endpoints implementing the same contract within the same service,
                // this method will be called twice. We only need to modify the schema once however.
                return;
            }

            foreach (var p in _requiredParameter)
            {
                var schemas = exporter.GeneratedXmlSchemas.Schemas(p.Namespace);

                var p2 = p;
                foreach (var complexType in from XmlSchema schema in schemas select (XmlSchemaElement)schema.Elements[p2.XmlQualifiedName] into message select message.ElementSchemaType as XmlSchemaComplexType)
                {
                    if (complexType == null) continue;
                    var sequence = complexType.Particle as XmlSchemaSequence;

                    if (sequence == null) continue;
                    var p1 = p;
                    foreach (var item in sequence.Items.Cast<XmlSchemaElement>().Where(item => item.Name == p1.Name))
                    {
                        item.MinOccurs = 1;
                        item.MinOccursString = "1";
                        break;
                    }
                }
            }

            // Throw away the temporary list we generated
            _requiredParameter = null;
        }

        #endregion

        #region Nested types

        private class RequiredMessagePart
        {
            public string Namespace { get; set; }
            public string Message { private get; set; }
            public string Name { get; set; }

            public XmlQualifiedName XmlQualifiedName
            {
                get
                {
                    return new XmlQualifiedName(Message, Namespace);
                }
            }
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter)]
    public class OptionalAttribute : Attribute
    {
    }
}