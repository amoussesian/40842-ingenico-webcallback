﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ININ.PSO.EMEA.WebCallback.Classes
{
    /// <summary>
    /// The class Schedules abstracts the database access for MS SQL Server or Oracle databases.
    /// The class exports four public methods for getting, inserting and updating Schedules for
    /// Callback requests.
    /// </summary>
    public class Schedules
    {
        private readonly string _connectionString;

        /// <summary>
        /// Open the connection to Oracle or MS SQL Server.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public Schedules(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Get all unprocessed schedules regardless of the database type.
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetSchedules()
        {
            var connection = new SqlConnection(_connectionString);
            var ds = new DataSet();
            DataTable dt;

            try
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CommandText = "dbo.PRC_PSO_GET_SCHEDULE";
                command.CommandType = CommandType.StoredProcedure;

                var da = new SqlDataAdapter(command);

                da.TableMappings.Add("PSO_CALLBACKSCHEDULES", "PSO_CALLBACKSCHEDULES");
                da.Fill(ds);

                dt = ds.Tables[0];
            }
            finally
            {
                // Close and Dispose MSSQLConnection
                connection.Close();
                connection.Dispose();
            }

            return dt;
        }

        /// <summary>
        /// Inserts Schedule into PSO_CALLBACKSCHEDULES table.
        /// </summary>
        /// <param name="scheduleDate">The schedule date.</param>
        /// <param name="number">The number.</param>
        /// <param name="queueName">Name of the queue.</param>
        /// <param name="message">The message.</param>
        /// <param name="attributes">The attributes.</param>
        /// <param name="customValue1"></param>
        public void InsertSchedule(DateTime scheduleDate, 
                                   string number, 
                                   string queueName, 
                                   string message, 
                                   string attributes, 
                                   string customValue1)
        {
            InsertSchedule(scheduleDate, 
                           0, 
                           number, 
                           queueName, 
                           message, 
                           attributes, 
                           String.Empty, 
                           customValue1);
        }


        /// <summary>
        /// Inserts Schedule into PSO_CALLBACKSCHEDULES table.
        /// </summary>
        /// <param name="scheduleDate">The schedule date.</param>
        /// <param name="immediate">The immediate.</param>
        /// <param name="number">The number.</param>
        /// <param name="queueName">Name of the queue.</param>
        /// <param name="message">The message.</param>
        /// <param name="attributes">The attributes.</param>
        /// <param name="callbackCallId">The callback call identifier.</param>
        /// <param name="customValue1"></param>
        public void InsertSchedule(DateTime scheduleDate, 
                                   int immediate, 
                                   string number, 
                                   string queueName,
                                   string message, 
                                   string attributes, 
                                   string callbackCallId, 
                                   string customValue1)
        {
            var connection = new SqlConnection(_connectionString);

            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText = "dbo.PRC_PSO_INS_SCHEDULE";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("SCHEDULEDATE", SqlDbType.DateTime).Value = scheduleDate;
                command.Parameters["SCHEDULEDATE"].Direction = ParameterDirection.Input;

                command.Parameters.Add("IMMEDIATLY", SqlDbType.Int).Value = immediate;
                command.Parameters["IMMEDIATLY"].Direction = ParameterDirection.Input;

                command.Parameters.Add("NUMBER", SqlDbType.VarChar).Value = number;
                command.Parameters["NUMBER"].Direction = ParameterDirection.Input;

                command.Parameters.Add("QUEUE", SqlDbType.VarChar).Value = queueName;
                command.Parameters["QUEUE"].Direction = ParameterDirection.Input;

                command.Parameters.Add("MESSAGE", SqlDbType.VarChar).Value = message;
                command.Parameters["MESSAGE"].Direction = ParameterDirection.Input;

                command.Parameters.Add("ATTRIBUTES", SqlDbType.VarChar).Value = attributes;
                command.Parameters["ATTRIBUTES"].Direction = ParameterDirection.Input;

                command.Parameters.Add("CALLBACKCALLID", SqlDbType.VarChar).Value = callbackCallId;
                command.Parameters["CALLBACKCALLID"].Direction = ParameterDirection.Input;

                command.Parameters.Add("CUSTOMVALUE1", SqlDbType.VarChar).Value = customValue1;
                command.Parameters["CUSTOMVALUE1"].Direction = ParameterDirection.Input;

                command.ExecuteNonQuery();
            }
            finally
            {
                // Close and Dispose OracleConnection
                connection.Close();
                connection.Dispose();
            }
        }

        /// <summary>
        /// UpdateSchedule updates a record identified by Id and sets the CallbackCallId (Callback Interaction Id).
        /// </summary>
        /// <param name="id">GUID of the Calback record´.</param>
        /// <param name="callbackCallId">Callback Interaction Id</param>
        /// <param name="success">Set to 1 if the creation was successful.</param>
        /// <returns>Database error messages</returns>
        public void UpdateSchedule(string id, string callbackCallId, int success)
        {
            var connection = new SqlConnection(_connectionString);

            try
            {
                connection.Open();
                var command = new SqlCommand("dbo.PRC_PSO_UPD_SCHEDULE", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.Add("ID", SqlDbType.VarChar).Value = id;
                command.Parameters["ID"].Direction = ParameterDirection.Input;

                command.Parameters.Add("CALLBACKCALLID", SqlDbType.VarChar).Value = callbackCallId;
                command.Parameters["CALLBACKCALLID"].Direction = ParameterDirection.Input;

                command.Parameters.Add("SUCCESS", SqlDbType.Int).Value = success;
                command.Parameters["SUCCESS"].Direction = ParameterDirection.Input;

                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
        }
    }
}
