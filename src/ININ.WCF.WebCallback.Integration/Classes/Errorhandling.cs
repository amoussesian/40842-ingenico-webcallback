﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace ININ.PSO.EMEA.WebCallback.Classes
{
    #region Error Attribute
    /// <summary>
    /// Used to catch uncaught exceptions and prevent channels from going into faulted state.
    /// </summary>
    public class FaultErrorHandler : IErrorHandler
    {
        #region IErrorHandler Members
        /// <summary>
        /// Logs a trace message in case an uncaught exception occurs.
        /// </summary>
        /// <param name="error"></param>
        /// <returns></returns>
        public bool HandleError(Exception error)
        {
            using (Tracing.Main.scope())
            {
                var f = error as FaultException;
                if (f != null) return false;
                Tracing.Main.exception(error);
                return true;
            }
        }
        /// <summary>
        /// Provides a fault message
        /// </summary>
        /// <param name="error"></param>
        /// <param name="version"></param>
        /// <param name="fault"></param>
        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
            if (fault == null)
            {
                var f = new FaultException(error.Message, FaultCode.CreateSenderFaultCode(null));
                var mf = f.CreateMessageFault();
                fault = Message.CreateMessage(version, mf, f.Action);
            }
        }
        #endregion
    }
    /// <summary>
    /// Applies the <see cref="FaultErrorHandler"/> logic to all channels
    /// </summary>
    public class ErrorHandlerAttribute : Attribute, IServiceBehavior
    {
        #region IServiceBehavior Members
        /// <summary>
        /// Not used
        /// </summary>
        /// <param name="serviceDescription"></param>
        /// <param name="serviceHostBase"></param>
        /// <param name="endpoints"></param>
        /// <param name="bindingParameters"></param>
        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }
        /// <summary>
        /// Applies the Error Hander to all channels
        /// </summary>
        /// <param name="serviceDescription"></param>
        /// <param name="serviceHostBase"></param>
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher dispatcher in serviceHostBase.ChannelDispatchers)
            {
                dispatcher.ErrorHandlers.Add(new FaultErrorHandler());
            }
        }
        /// <summary>
        /// Not used
        /// </summary>
        /// <param name="serviceDescription"></param>
        /// <param name="serviceHostBase"></param>
        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }
        #endregion
    }
    #endregion
}
