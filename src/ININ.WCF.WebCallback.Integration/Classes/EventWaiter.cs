﻿using System;
using System.Reflection;
using System.Threading;

namespace ININ.PSO.EMEA.WebCallback.Classes
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EventWaiter<T> where T : EventArgs
    {
        /// <summary>
        /// The _auto reset event
        /// </summary>
        private readonly AutoResetEvent _autoResetEvent;

        /// <summary>
        /// The _event
        /// </summary>
        private readonly EventInfo _event;

        /// <summary>
        /// The _event container
        /// </summary>
        private readonly object _eventContainer;

        /// <summary>
        /// The _request identifier
        /// </summary>
        private readonly string _requestId;

        /// <summary>
        /// Initializes a new instance of the <see cref="EventWaiter{T}"/> class.
        /// </summary>
        /// <param name="eventContainer">The event container.</param>
        /// <param name="eventName">Name of the event.</param>
        /// <param name="requestId">The request identifier.</param>
        public EventWaiter(object eventContainer, string eventName, string requestId)
        {
            _autoResetEvent = new AutoResetEvent(false);

            _requestId = string.Empty;

            _requestId = requestId;

            _eventContainer = eventContainer;

            _event = eventContainer.GetType().GetEvent(eventName);
        }

        /// <summary>
        /// The _request identifier
        /// </summary>
        public string RequestId
        {
            get { return _requestId; }
        }

        /// <summary>
        /// Waits for event.
        /// </summary>
        /// <param name="timeout">The timeout.</param>
        /// <returns></returns>
        public object WaitForEvent(TimeSpan timeout)
        {
            object eventArgs = null;

            var eventHandler = new EventHandler<T>((sender, args) =>
            {
                eventArgs = args;

                if (_autoResetEvent != null)
                    _autoResetEvent.Set();
            });

            _event.AddEventHandler(_eventContainer, eventHandler);

            _autoResetEvent.WaitOne(timeout);

            _event.RemoveEventHandler(_eventContainer, eventHandler);

            return eventArgs;
        }
    }
}
