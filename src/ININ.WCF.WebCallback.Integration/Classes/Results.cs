﻿namespace ININ.PSO.EMEA.WebCallback.Classes
{
    /// <summary>
    /// Standard response for the <see cref="IScheduleCallbackService.CreateCallback"/> method.<br/>
    /// </summary>
    public class CreateCallbackResponse
    {
        /// <summary>
        /// Reference to the Callback
        /// </summary>
// ReSharper disable once InconsistentNaming
        public string CallbackId { get; set; }
    }
}
