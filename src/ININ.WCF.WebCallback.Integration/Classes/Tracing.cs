﻿using System;

namespace ININ.PSO.EMEA.WebCallback.Classes
{
    /// <summary>
    /// Represents a <see cref="TopicTracer"/> with a specific topic.
    /// If this class fails, the application will immediately and entierly crash.
    /// </summary>
    public class MainTopic : TopicTracer
    {
        /// <summary>
        /// TopicTracer handle
        /// </summary>
        private readonly int _hdl;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainTopic"/> class.
        /// </summary>
        /// <param name="topic">The topic name.</param>
        public MainTopic(string topic)
        {
            _hdl = I3Trace.initialize_topic(topic, 100);
        }

        /// <summary>
        /// Get_handles this instance.
        /// </summary>
        /// <returns></returns>
        public override int get_handle()
        {
            return _hdl;
        }
    }

    /// <summary>
    /// Provides static access to all tracing.
    /// </summary>
    public static class Tracing
    {
        /// <summary>
        /// The main topic
        /// </summary>
        public static MainTopic Main { get; private set; }

        /// <summary>
        /// The cic topic
        /// </summary>
        public static MainTopic CIC { get; private set; }

        /// <summary>
        /// Initializes the specified topic.
        /// </summary>
        /// <param name="topic">The topic name.</param>
        public static void Initialize(string topic)
        {
            try
            {
                Main = new MainTopic(topic);

                CIC = new MainTopic(topic + ".CIC");

                I3Trace.initialize_default_sinks();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
