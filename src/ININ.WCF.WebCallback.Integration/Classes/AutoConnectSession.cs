﻿using System;
using ININ.IceLib.Connection;

namespace ININ.PSO.EMEA.WebCallback.Classes
{
    /// <summary>
    /// 
    /// </summary>
    public enum CicAuthenicationMode
    {
        /// <summary>
        /// Unknown mode
        /// </summary>
        Unknown = 1,
        /// <summary>
        /// IC user authentication
        /// </summary>
        IcAuth = 2,
        /// <summary>
        /// Windows authentication
        /// </summary>
        WindowsAuth = 3,
    }

    /// <summary>
    /// 
    /// </summary>
    public sealed class AutoConnectSession
    {
        /// <summary>
        /// 
        /// </summary>
        static private volatile AutoConnectSession _AutoConnectSession;

        /* TODO
        /// <summary>
        /// 
        /// </summary>
        private static SettingCategory _settingCategory;
        */

        /// <summary>
        /// 
        /// </summary>
        static private readonly object SYNC_ROOT = new Object();

        /// <summary>
        /// 
        /// </summary>
        static private volatile Session _Session;

        /// <summary>
        /// 
        /// </summary>
        static private readonly object SYNC_SESSION = new Object();

        /// <summary>
        /// 
        /// </summary>
        private ConnectionStateReason? _priorConnectionDownStateReason;

        /// <summary>
        /// 
        /// </summary>
        private readonly string _cicUserName;

        /// <summary>
        /// 
        /// </summary>
        private readonly string _cicUserPassword;

        /// <summary>
        /// 
        /// </summary>
        private readonly string _cicHost;

        /// <summary>
        /// 
        /// </summary>
        private readonly CicAuthenicationMode _cicAuthenticationMode;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<EventArgs> OnConnected;

        /// <summary>
        /// Prevents a default instance of the <see cref="AutoConnectSession"/> class from being created.
        /// </summary>
        /// <param name="cicHost">The cic host.</param>
        /// <param name="cicUserName">Name of the cic user.</param>
        /// <param name="cicUserPassword">The cic user password.</param>
        private AutoConnectSession(string cicHost, string cicUserName, string cicUserPassword)
        {
            using (Tracing.CIC.scope())
            {
                try
                {
                    _cicHost = cicHost;

                    _cicUserName = cicUserName;

                    _cicUserPassword = cicUserPassword;

                    _cicAuthenticationMode = CicAuthenicationMode.IcAuth;

                    Tracing.CIC.note("CICAuthenticationMode={}.", _cicAuthenticationMode.ToString());

                    if (_Session == null)
                    {
                        Tracing.CIC.note("Creating new ICELib Session.");

                        _Session = new Session { AutoReconnectEnabled = true };

                        _Session.ConnectionStateChanged += SessionConnectionStateChanged;
                    }
                }
                catch (Exception ex)
                {
                    Tracing.CIC.exception(ex);
                }
            }

        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <returns></returns>
        public static AutoConnectSession GetInstance()
        {
            using (Tracing.CIC.scope())
            {
                try
                {
                    if (_AutoConnectSession == null)
                    {
                        lock (SYNC_ROOT)
                            if (_AutoConnectSession == null)
                            {
                                Tracing.CIC.status("Creating AutoConnectSession instance.");

                                _AutoConnectSession =
                                    new AutoConnectSession(
                                        System.Configuration.ConfigurationManager.AppSettings.Get("ICServer"),
                                        System.Configuration.ConfigurationManager.AppSettings.Get("ICUsername"),
                                        System.Configuration.ConfigurationManager.AppSettings.Get("ICPassword"));
                            }
                    }
                    else
                        Tracing.CIC.note("Using existing AutoConnectSession instance.");
                }
                catch (Exception ex)
                {
                    Tracing.CIC.exception(ex);
                }
            }

            return _AutoConnectSession;
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="traceTopic">The trace topic.</param>
        /// <returns></returns>
        public static AutoConnectSession GetInstance(string traceTopic)
        {
            Tracing.Initialize(traceTopic);

            return GetInstance();
        }

        /// <summary>
        /// Called when [connected].
        /// </summary>
        private void InvokeOnConnected()
        {
            var handler = OnConnected;

            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Gets the current session.
        /// </summary>
        public Session CurrentSession
        {
            get
            {
                using (Tracing.CIC.scope())
                {
                    try
                    {
                        lock (SYNC_SESSION)
                            if (_Session == null)
                            {
                                Tracing.CIC.status("Creating new ICELib Session object.");

                                _Session = new Session { AutoReconnectEnabled = true };

                                _Session.ConnectionStateChanged += SessionConnectionStateChanged;

                                if (_Session.ConnectionState != ConnectionState.Up &&
                                    _Session.ConnectionState != ConnectionState.Attempting)
                                {
                                    Tracing.CIC.status("Establishing ICELib Session to CICHost '{}'.",
                                                       _AutoConnectSession._cicHost);
                                    _AutoConnectSession.ConnectSession();
                                }
                            }
                    }
                    catch (Exception ex)
                    {
                        Tracing.CIC.exception(ex);
                    }
                }
                return _Session;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="data">The <see cref="CustomConnectionStateChangeEventArgs"/> instance containing the event data.</param>
        public delegate void ConnectionStateChangeHandler(object sender, CustomConnectionStateChangeEventArgs data);

        /// <summary>
        /// Occurs when [connection state change].
        /// </summary>
        public event ConnectionStateChangeHandler ConnectionStateChange;

        /// <summary>
        /// Connects the session.
        /// </summary>
        public void ConnectSession()
        {
            using (Tracing.CIC.scope())
            {
                try
                {
                    if (_Session == null)
                        return;

                    if (_Session.ConnectionState == ConnectionState.Up
                        || _Session.ConnectionState == ConnectionState.Attempting)
                        return;

                    Tracing.CIC.status("Establishing ICELib Session to CICHost '{}', user '{}'.",
                                       _AutoConnectSession._cicHost, _AutoConnectSession._cicUserName);

                    switch (_cicAuthenticationMode)
                    {
                        case CicAuthenicationMode.IcAuth:

                            Tracing.CIC.always("Calling Session.Connect with CICAuthenticationMode='{}'",
                                               _cicAuthenticationMode.ToString());

                            _Session.Connect(new SessionSettings(),
                                             new HostSettings(new HostEndpoint(_cicHost)),
                                             new ICAuthSettings(_cicUserName, _cicUserPassword),
                                             new StationlessSettings());
                            break;

                        case CicAuthenicationMode.WindowsAuth:
                            Tracing.CIC.always("Calling Session.Connect with CICAuthenticationMode='{}'",
                                               _cicAuthenticationMode.ToString());

                            _Session.Connect(new SessionSettings(),
                                             new HostSettings(new HostEndpoint(_cicHost)),
                                             new WindowsAuthSettings(),
                                             new StationlessSettings());
                            break;

                        default:
                            throw new NotSupportedException(
                                    "This service only supports ICAuthSettings or WindowsAuthSettings for an IC Session.");
                    }

                    // TODO: If not connected, possibly attempt to reconnect via 
                    //  A) Recursive call to this method up to reconnect limit
                    //  B) On timer interval up to reconnect limit
                }
                catch (Exception ex)
                {
                    Tracing.CIC.exception(ex);
                }
            }
        }

        /// <summary>
        /// Sessions the connection state changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ININ.IceLib.Connection.ConnectionStateChangedEventArgs"/> instance containing the event data.</param>
        private void SessionConnectionStateChanged(object sender, ConnectionStateChangedEventArgs e)
        {
            using (Tracing.CIC.scope())
            {
                try
                {
                    string tmpPriorReason = "NULL";

                    if (_priorConnectionDownStateReason.HasValue)
                        tmpPriorReason = _priorConnectionDownStateReason.ToString();

                    Tracing.CIC.status("ConnectionState={}, PriorConnectionDownStateReason={}.", e.State,
                                         tmpPriorReason);

                    if (e.State == ConnectionState.Up)
                    {
                        InvokeOnConnected();

                        if (_priorConnectionDownStateReason != null)
                        {
                            switch (_priorConnectionDownStateReason)
                            {
                                case ConnectionStateReason.Switchover:
                                case ConnectionStateReason.ServerNotResponding:
                                case ConnectionStateReason.AdminLogOff:
                                case ConnectionStateReason.UserLogOff:
                                case ConnectionStateReason.AnotherLogOn:
                                case ConnectionStateReason.LogOnFailed:
                                case ConnectionStateReason.LogOn:
                                case ConnectionStateReason.None:
                                case ConnectionStateReason.UserDeleted:
                                case ConnectionStateReason.SessionTimeout:
                                case ConnectionStateReason.StationDeactivated:
                                case ConnectionStateReason.StationDeleted:
                                    OnConnectionStateChange(
                                            new CustomConnectionStateChangeEventArgs(_priorConnectionDownStateReason,
                                                                                     CustomConnectionState.Up));
                                    break;

                                default:
                                    OnConnectionStateChange(new CustomConnectionStateChangeEventArgs(null,
                                                                                                     CustomConnectionState
                                                                                                             .Up));
                                    break;
                            }
                        }
                    }
                    else if (e.State == ConnectionState.Down)
                    {
                        _priorConnectionDownStateReason = e.Reason;

                        Tracing.CIC.always("Setting PriorConnectionDownStateReason={}.", e.Reason);

                        OnConnectionStateChange(new CustomConnectionStateChangeEventArgs(null, CustomConnectionState.Down));

                        switch (e.Reason)
                        {
                            case ConnectionStateReason.Switchover:
                            case ConnectionStateReason.ServerNotResponding:
                                throw new Exception(
                                        String.Format(
                                                "Connection is down due to {0}. The service will attempt to recover when the connection is restored. Contact your CIC Administrator.",
                                                e.Reason));
                            case ConnectionStateReason.AdminLogOff:
                            case ConnectionStateReason.UserLogOff:
                            case ConnectionStateReason.AnotherLogOn:
                            case ConnectionStateReason.LogOnFailed:
                            case ConnectionStateReason.UserDeleted:
                            case ConnectionStateReason.SessionTimeout:
                            case ConnectionStateReason.StationDeactivated:
                            case ConnectionStateReason.StationDeleted:
                            case ConnectionStateReason.None:
                                throw new Exception(
                                        String.Format(
                                                "Connection is down due to {0}. The service is unable to perform its essential functions. Contact your CIC Administrator.",
                                                e.Reason));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Tracing.CIC.exception(ex);

                    throw;
                }
                finally
                {
                    if (e.State == ConnectionState.Up && _priorConnectionDownStateReason != null)
                    {
                        Tracing.CIC.always("Connection is up. Setting PriorConnectionDownStateReason to null.");

                        _priorConnectionDownStateReason = null;
                    }
                }
            }
        }

        private void OnConnectionStateChange(CustomConnectionStateChangeEventArgs data)
        {
            if (ConnectionStateChange != null)
                ConnectionStateChange(this, data);
        }
    }


    /// <summary>
    /// 
    /// </summary>
    public enum CustomConnectionState
    {
        /// <summary>
        /// The unknown state
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Down
        /// </summary>
        Down = 1,
        /// <summary>
        /// Up
        /// </summary>
        Up = 2
    }

    /// <summary>
    /// 
    /// </summary>
    public class CustomConnectionStateChangeEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the prior connection down state reason.
        /// </summary>
        public ConnectionStateReason? PriorConnectionDownStateReason { get; internal set; }

        /// <summary>
        /// Gets the state of the connection.
        /// </summary>
        /// <value>
        /// The state of the connection.
        /// </value>
        public CustomConnectionState ConnectionState { get; internal set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomConnectionStateChangeEventArgs"/> class.
        /// </summary>
        /// <param name="priorConnectionDownStateReason">The prior connection down state reason.</param>
        /// <param name="connectionState">State of the connection.</param>
        public CustomConnectionStateChangeEventArgs(ConnectionStateReason? priorConnectionDownStateReason,
                                                    CustomConnectionState connectionState)
        {
            PriorConnectionDownStateReason = priorConnectionDownStateReason;

            ConnectionState = connectionState;
        }
    }
}
