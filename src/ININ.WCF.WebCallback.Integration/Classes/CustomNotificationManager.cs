﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using ININ.IceLib.Connection;
using ININ.IceLib.Connection.Extensions;

namespace ININ.PSO.EMEA.WebCallback.Classes
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class CustomNotificationManager
    {
        /// <summary>
        /// 
        /// </summary>
        static private readonly object SYNC_ROOT = new Object();

        /// <summary>
        /// 
        /// </summary>
        private volatile Session _session;

        /// <summary>
        /// 
        /// </summary>
        private volatile static CustomNotificationManager _Instance;

        /// <summary>
        /// Gets or sets the custom notification object ids.
        /// </summary>
        /// <value>
        /// The custom notification object ids.
        /// </value>
        public List<string> CustomNotificationObjectIds { [MethodImpl(MethodImplOptions.Synchronized)] get; [MethodImpl(MethodImplOptions.Synchronized)] set; }

        /// <summary>
        /// Gets the watched notification.
        /// </summary>
        /// <value>
        /// The watched notification.
        /// </value>
        public CustomNotification WatchedNotification { get; private set; }

        /// <summary>
        /// Occurs when [on custom notification received].
        /// </summary>
        public event EventHandler<CustomNotificationEventArgs> OnCustomNotificationReceived;

        /// <summary>
        /// Prevents a default instance of the <see cref="CustomNotificationManager"/> class from being created.
        /// </summary>
        /// <param name="session">The session.</param>
        public CustomNotificationManager(Session session)
        {
            using (Tracing.CIC.scope())
            {
                try
                {
                    Tracing.CIC.note("Trying to initialize CustomNotificationManager.");

                    if (session != null)
                    {
                        _session = session;

                        WatchedNotification = new CustomNotification(_session);

                        WatchedNotification.CustomNotificationReceived += CustomNotificationReceived;
                    }
                    else
                        Tracing.CIC.warning("No IceLib session found initialization aborted.");
                }
                catch (Exception ex)
                {
                    Tracing.CIC.exception(ex);
                }
            }
        }

        /// <summary>
        /// Starts the watching.
        /// </summary>
        /// <param name="customNotificationReceiveList">The custom notification receive list.</param>
        public void StartWatching(List<string> customNotificationReceiveList)
        {
            using (Tracing.CIC.scope())
            {
                if (_session == null) return;

                if (_session.ConnectionState == ConnectionState.Up)
                {
                    Tracing.CIC.status("Starting to watch the following object ids:{}", Environment.NewLine,
                        customNotificationReceiveList.Aggregate(String.Empty, (s, s1) => s + Environment.NewLine + s1));

                    CustomNotificationObjectIds = customNotificationReceiveList;

                    var count = CustomNotificationObjectIds.Count;

                    var customMessageHeaderList = new CustomMessageHeader[count];

                    for (var index = 0; index < count; index++)
                        customMessageHeaderList[index] =
                                new CustomMessageHeader(CustomMessageType.ServerNotification,
                                                        CustomNotificationObjectIds[index], _session.UserId);

                    WatchedNotification.StartWatching(customMessageHeaderList);
                }
                else
                    Tracing.CIC.warning("No IceLib connection. Aborting...");
            }
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="session">The session.</param>
        /// <param name="customNotificationReceiveList">The custom notification receive list.</param>
        /// <returns></returns>
        public static CustomNotificationManager GetInstance(Session session, List<string> customNotificationReceiveList)
        {
            using (Tracing.CIC.scope())
            {
                try
                {
                    if (_Instance == null)
                    {
                        Tracing.CIC.note("Creating CustomNotificationManager instance.");

                        lock (SYNC_ROOT)
                        {
                            _Instance = new CustomNotificationManager(session);
                        }
                    }
                    else
                        Tracing.CIC.note("Using existing CustomNotificationManager instance.");
                }
                catch (Exception ex)
                {
                    Tracing.CIC.exception(ex);
                }

                return _Instance;
            }
        }

        /// <summary>
        /// Invokes the on custom notification received.
        /// </summary>
        /// <param name="e">The instance containing the event data.</param>
        public void InvokeOnCustomNotificationReceived(CustomNotificationEventArgs e)
        {
            using (Tracing.CIC.scope())
            {
                try
                {
                    var handler = OnCustomNotificationReceived;

                    if (handler != null)
                        handler(this, e);
                }
                catch (Exception ex)
                {
                    Tracing.CIC.exception(ex);
                }
            }
        }

        /// <summary>
        /// Sends the custom notification request.
        /// </summary>
        /// <param name="objectId">The object id.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public bool SendCustomNotificationRequest(string objectId, string[] parameters)
        {
            using (Tracing.CIC.scope())
            {
                try
                {
                    Tracing.CIC.note("Trying to send custom notification request: '" + objectId + "'.");

                    if (_session.ConnectionState == ConnectionState.Up)
                    {
                        var customNotificationSend = new CustomNotification(_session);

                        var request =
                                new CustomRequest(new CustomMessageHeader(CustomMessageType.ServerNotification, objectId,
                                                                          _session.UserId));

                        request.Write(parameters);

                        customNotificationSend.SendServerRequestNoResponse(request);
                    }
                    else
                        Tracing.CIC.warning("No IceLib connection. Aborting...");
                }
                catch (Exception ex)
                {
                    Tracing.CIC.exception(ex);
                }

                return false;
            }
        }

        /// <summary>
        /// Customs the notification received.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ININ.IceLib.Connection.Extensions.CustomNotificationReceivedEventArgs"/> instance containing the event data.</param>
        private void CustomNotificationReceived(object sender, CustomNotificationReceivedEventArgs e)
        {
            using (Tracing.CIC.scope())
            {
                try
                {
                    var objectId = e.Message.ObjectId;

                    Tracing.CIC.note("Custom notification received. ObjectId = '" + objectId + "'");

                    var strings = e.Message.ReadStrings();

                    var expectedType = typeof(string);

                    if (strings != null)
                    {
                        if (ReferenceEquals(strings.GetType(), expectedType.MakeArrayType()))
                        {
                            foreach (var s in strings)
                                Tracing.CIC.note("Notification return value:'" + s + "'.");

                            InvokeOnCustomNotificationReceived(new CustomNotificationEventArgs(true, strings, objectId));

                            return;
                        }

                        Tracing.CIC.note("Notification return values are not of type string.");

                        InvokeOnCustomNotificationReceived(new CustomNotificationEventArgs(false, new string[] { },
                                                                                           objectId));

                        return;
                    }

                    Tracing.CIC.note("Notification has no return values.");

                    InvokeOnCustomNotificationReceived(new CustomNotificationEventArgs(false, new string[] { }, objectId));

                    return;
                }
                catch (Exception ex)
                {
                    Tracing.CIC.exception(ex);
                }

                InvokeOnCustomNotificationReceived(new CustomNotificationEventArgs(false, new string[] { }, ""));
            }
        }

        /// <summary>
        /// Stops the watching.
        /// </summary>
        public void StopWatching()
        {
            using (Tracing.CIC.scope())
            {
                if (WatchedNotification != null && WatchedNotification.IsWatching())
                {
                    WatchedNotification.StopWatching();
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class CustomNotificationEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CustomNotificationEventArgs"/> is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public string[] Result { get; set; }

        /// <summary>
        /// Gets or sets the object id.
        /// </summary>
        /// <value>
        /// The object id.
        /// </value>
        public string ObjectId { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomNotificationEventArgs"/> class.
        /// </summary>
        /// <param name="success">if set to <c>true</c> [success].</param>
        /// <param name="result">The result.</param>
        /// <param name="objectId">The object id.</param>
        public CustomNotificationEventArgs(bool success, string[] result, string objectId)
        {
            Success = success;

            Result = result;

            ObjectId = objectId;
        }
    }
}
