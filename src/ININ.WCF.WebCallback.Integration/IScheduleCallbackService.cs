﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using ININ.PSO.EMEA.WebCallback.Classes;
using WCFExtras.Wsdl.Documentation;

namespace ININ.PSO.EMEA.WebCallback
{
    /// <summary>
    /// Parameters used in <see cref="IScheduleCallbackService.CreateCallbackSchedule"/> and <see cref="IScheduleCallbackService.CreateCallback"/>.
    /// </summary>
    [DataContract]
    public class CallbackParameters
    {
        /// <summary>
        /// The scoped queue name
        /// </summary>
        [DataMember]
        public string ScopedQueueName { get; set; }
        /// <summary>
        /// The number
        /// </summary>
        [DataMember]
        public string Number { get; set; }
        /// <summary>
        /// The date time
        /// </summary>
        [DataMember]
        public string DateTime { get; set; }
        /// <summary>
        /// The message
        /// </summary>
        [DataMember]
        public string Message { get; set; }
        /// <summary>
        /// Gets or sets the custom value1.
        /// </summary>
        /// <value>
        /// The custom value1.
        /// </value>
        [DataMember]
        public string CustomValue1 { get; set; }
        /// <summary>
        /// The attributes
        /// </summary>
        [DataMember]
        public Dictionary<string, string> Attributes { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CallbackParameters"/> class.
        /// </summary>
        public CallbackParameters()
        {
            Attributes = new Dictionary<string, string>();
        }
    }

    /// <summary>
    /// Main Interface for the WCF Service
    /// </summary>
    [XmlComments]
    [ServiceContract(Namespace = "http://inin.com")]
    [RequiredParametersBehavior]
    public interface IScheduleCallbackService
    {
        /// <summary>
        /// Create a Callback schedule entry in the Table CallbackSchedules. A server component will frequently scan (set by a configurable parameter) the table CallbackSchedules looking for callbacks to initiate. If so it creates the required callbacks in the corresponding workgroup queue.
        /// </summary>
        /// <param name="parameters">The <see cref="CallbackParameters"/> used for scheduling a callback.</param>
        /// <returns>
        /// Reference to the Callback
        /// </returns>
        [OperationContract]
        string CreateCallbackSchedule(CallbackParameters parameters);

        /// <summary>
        /// Create a Callback at the agent queue. If the agent is unable to handle a Callback simultaneously it creates the required callback in the corresponding workgroup queue instead.
        /// </summary>
        /// <param name="parameters">The <see cref="CallbackParameters"/>.</param>
        /// <returns>
        /// A <see cref="CreateCallbackResponse" /> object.
        /// </returns>
        [OperationContract]
        CreateCallbackResponse CreateCallback(CallbackParameters parameters);
    }
}
