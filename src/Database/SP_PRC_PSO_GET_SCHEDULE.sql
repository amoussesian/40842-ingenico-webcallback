SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRC_PSO_GET_SCHEDULE] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @Date datetime;
	
	SET @Date = GETUTCDATE();
	
	DECLARE @hostname varchar(50);

	SELECT @hostname = hostname FROM sys.sysprocesses WHERE spid = @@SPID
	
    -- Insert statements for procedure here
	SELECT *
            FROM  [dbo].[PSO_CALLBACKSCHEDULES]
            WHERE [SUCCESS] = 0
            AND [IMMEDIATLY] = 0
            AND [SCHEDULEDATE] <= @Date
            AND [TRIES] <= 100
	UPDATE 
		[dbo].[PSO_CALLBACKSCHEDULES]
	SET
		[SUCCESS] = 1,
		[TRIES] = [TRIES] + 1
	WHERE [SUCCESS] = 0
        AND [IMMEDIATLY] = 0
        AND [SCHEDULEDATE] <= @Date
        AND [TRIES] <= 100
END

GO


