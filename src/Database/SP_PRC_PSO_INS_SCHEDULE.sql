SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRC_PSO_INS_SCHEDULE]
	-- Add the parameters for the stored procedure here
	@SCHEDULEDATE DATETIME,
	@IMMEDIATLY BIT,
	@NUMBER VARCHAR(50),
    @QUEUE VARCHAR(50),
	@MESSAGE VARCHAR(1000),
	@ATTRIBUTES VARCHAR(1000),
    @CALLBACKCALLID VARCHAR(50),
	@CUSTOMVALUE1 VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	DECLARE @hostname varchar(50);

	SELECT @hostname = hostname FROM sys.sysprocesses WHERE spid = @@SPID

    -- Insert statements for procedure here
	INSERT INTO [dbo].[PSO_CALLBACKSCHEDULES]
           ([SCHEDULEDATE]
           ,[IMMEDIATLY]
           ,[NUMBER]
           ,[QUEUE]
		   ,[MESSAGE]
		   ,[ATTRIBUTES]
           ,[CALLBACKCALLID]
           ,[CUSTOMVALUE1]
           ,[SUCCESS]
           ,[TRIES]
           ,[CREATEDATE]
           ,[CREATE_HOST])
     VALUES
           (@SCHEDULEDATE
           ,@IMMEDIATLY
           ,@NUMBER
           ,@QUEUE
           ,@MESSAGE
		   ,@ATTRIBUTES
           ,@CALLBACKCALLID
           ,@CUSTOMVALUE1
           ,0
           ,0
           ,GETUTCDATE()
           ,@hostname)
END


GO


