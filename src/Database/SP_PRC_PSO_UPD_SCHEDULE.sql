SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRC_PSO_UPD_SCHEDULE]
	-- Add the parameters for the stored procedure here
	@ID   int,
    @CALLBACKCALLID  varchar(50),
    @SUCCESS         bit
    AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @hostname varchar(50);

	SELECT @hostname = hostname FROM sys.sysprocesses WHERE spid = @@SPID


    -- Insert statements for procedure here
	 UPDATE [dbo].PSO_CALLBACKSCHEDULES
        SET CALLBACKCALLID =  @CALLBACKCALLID,
            SUCCESS = @SUCCESS,
            UPDATEDATE = GETUTCDATE(), 
            UPDATE_HOST = @hostname
        WHERE ID = @ID;
END

GO


