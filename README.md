# WebCallback for Ingenico #

ININ.EMEA.WebCallback is a web service that can be used to create and schedule callbacks in CIC.
It features one function to create a callback and one function to schedule a callback.

## Requirements ##
* Dedicated web server (any Windows Server 2003+ which is not the CIC server) with .NET Framework 4+.
* CIC 4.0+ with Icelib SDK feature license.
* CIC user with master administration rights.
* Dedicated MS SQL database server (SQL Server 2008+) and a SQL Server user having execution rights for stored procedures.
* The web server hosting the service will need access to the IC Server and the database server.

## Installation Package Content ##
This document: ININ_EMEA_WebCallback_InstallConfigGuide
### Folder Setup: ###
* WebCallbackSetup.msi: the Windows Service installer package
* Setup.exe: the executable for the Windows Service installer
### Folder SQL ###
* PSO_CALLBACKSCHEDULES.sql : SQL statement creating the table used to store scheduled callbacks
* SP_PRC_PSO_GET_SCHEDULE.sql : SQL statement creating the stored procedure used to get current scheduled callbacks
* SP_PRC_PSO_INS_SCHEDULE.sql : SQL statement creating the stored procedure used to insert a scheduled callback
* SP_PRC_PSO_UPD_SCHEDULE.sql : SQL statement creating the stored procedure used to update a scheduled callback
### Folder Handler ###
* ININ_EMEA_CheckPhoneNumber

## Installation Procedures ##
1.	Create all stored procedures and tables in the desired database with the SQL statements from folder SQL
2.	Install the IceLib SDK 4.0
3.	Run the installer: Setup.exe (in folder Setup)
4.	Publish handlers
5.	Set the configuration settings (see Configuration Instructions below)

## Web Service Configuration ##
ININ.PSO.EMEA.WebCallback.Service.exe.config – This file is found in the folder where the Windows Service has been installed.

* ICUsername: The name of an IC user in which to connect to CIC server with. This can be a custom username for only the web service.
* ICPassword: The password for the above IC user.
* ICServer: The name of the IC server.
* SQLConnectionString: A connection string to the SQL Server database which contains the PSO_CALLBACKSCHEDULES table and all stored procedures.
* CallbackWorkgroup: Currently, this setting is not used.
* DatabaseAccessInterval: The interval at which the web service pulls scheduled callbacks from the database. Per default, this is 10 seconds (meaning that the maximum delay for a scheduled callback to be put on queue is 10 seconds).

## Web.config ##


```
#!xml

<appSettings>
    <add key="ICUsername" value="<IC user ID>" />
    <add key="ICPassword" value="<IC password>" />
    <add key="ICServer" value="<IC server IP address or server name>" />
    <add key="SQLConnectionString" value="Data Source=<sql server name>;Initial Catalog=<database name>;Persist Security Info=True;User ID=<SQL server user ID>;Password=<SQL server password>" />
    <add key="CallbackWorkgroup" value="Currently not used" />
    <add key="DatabaseAccessInterval" value="10"/>
</appSettings
```
>



## CreateCallback ##
The web service exposes the function CreateCallback with the parameters described below.
CreateCallback creates an immediate callback in the specified queue with the specified parameters and attributes.

## CreateCallbackSchedule ##
The web service exposes the function CreateCallbackSchedule which allows scheduling a callback at a certain date and time in the UTC format. The scheduled callbacks are stored in a database table which allows reporting on them. The database table is described below.
By default, the database table is read at intervals of 10 seconds, so the callbacks are created with a maximum delay of 10 seconds from the scheduled time. See below for a description on how to configure this.